/**
 Memebers of this group:
 Jonathan E. Dawson - 217203768
 Dileep Kumar Tentu - 217205583
 Vishal Kheni - 217205316
 Moulali Penchikalapalli  - 217205137
 Sweetyben Vekariya- 216206022
 
 This C++ code implments the sequential and parallel computing algorithm for multiplication of three random matrices (A, B and C).
 The size of the matrices are 1000 by 1000.
 Computation carried using both sequential or parallel computing and total time taken in each case is evaluated and output.
 The final result of multiplication is stored in matrix E.
 
 **/

#include<iostream>
#include<stdlib.h>
#include<omp.h>
#include <time.h>
#include <fstream>

using namespace std;

#define m 1000
#define n 1000
#define p 1000


int main(int argc, char* argv[])
{
    float a[n][m],b[m][p],c[p][n],d[n][p],e[n][n];   // floating type for matrix element to allow for decimal numbers between 0 and 1
    srand(1122);     // initialize the seed for the random number generator
    float k;
    
    double Ttotal_seq=0;
    double Ttotal_par=0;
    double time_taken=0;
    double faster=0;
    double omp_get_wtime(void);
    int nthreads;
    
    clock_t t1_seq, t2_seq;
    // Sequential code
    for(int i=0;i<n;i++){
        for(int j=0;j<m;j++){
            k = rand() % 100;                       // generate random number in the range 0 to 99
            a[i][j] = k/100;                        // generate random number in the range 0 to 1
        }
    }
    
    for(int i=0;i<m;i++){
        for(int j=0;j<p;j++){
            k = rand() % 100;
            b[i][j] = k/100;
        }
    }
    
    for(int i=0;i<p;i++){
        for(int j=0;j<n;j++){
            k = rand() % 100;
            c[i][j] = k/100;
        }
    }
    for(int i=0;i<n;i++){
        for(int j=0;j<p;j++){
            d[i][j]=0;
        }
    }
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            e[i][j]=0;
        }
    }
    
    t1_seq=clock();                                             // start recording the time taken for core multiplication
    //sequential multiplication
    for(int i=0;i<n;i++){
        for(int k=0;k<p;k++){
            for(int j=0;j<m;j++){
                d[i][k]+=a[i][j]*b[j][k];
            }
        }
    }
    
    
    for(int i=0;i<n;i++){
        for(int k=0;k<n;k++){
            for(int j=0;j<p;j++){
                e[i][k]+=d[i][j]*c[j][k];
            }
        }
    }
    t2_seq=clock();                                           // stop recording the time taken for core multiplication
    
    Ttotal_seq=t2_seq-t1_seq;                                 // total time taken for core multiplication
    /**
     for(int i=0;i<n;i++){
     for(int j=0;j<n;j++){
     //cout<< e[i][j]<<" ";
     }
     
     cout<<endl;
     }
     **/
    
    time_taken= double  (Ttotal_seq)/(CLOCKS_PER_SEC);
    cout<<"\e[1m CPU Time (Sequential code) \e[0m"<<time_taken;
    cout<<endl;
    
    
    double t1_par=0;                            // declare and initialize the parameters for recording computing time
    double t2_par=0;
    
    // Parallel code starts below this line
#pragma omp parallel                                    // parallelization starts
    {
        
#pragma omp for                                        // parallelizing the 'for' loops
        
        for(int i=0;i<n;i++){
            for(int j=0;j<m;j++){
                k = rand() % 100;
                a[i][j] = k/100;
            }
        }
        
        for(int i=0;i<m;i++){
            for(int j=0;j<p;j++){
                k = rand() % 100;
                b[i][j] = k/100;
            }
        }
        
        for(int i=0;i<p;i++){
            for(int j=0;j<n;j++){
                k = rand() % 100;
                c[i][j] = k/100;
            }
        }
        for(int i=0;i<n;i++){
            for(int j=0;j<p;j++){
                d[i][j]=0;
            }
        }
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                e[i][j]=0;
            }
        }
        t1_par=omp_get_wtime();                                         // start recording the time taken for core multiplication
#pragma omp for
        
        for(int i=0;i<n;i++){
            for(int k=0;k<p;k++){
                for(int j=0;j<m;j++){
                    d[i][k]+=a[i][j]*b[j][k];                          // intermediate result in D= A*B
                }
            }
        }
#pragma omp for
        
        for(int i=0;i<n;i++){
            for(int k=0;k<n;k++){
                nthreads = omp_get_num_threads();                       // obtain the number of threads involved in computation
                for(int j=0;j<p;j++){
                    e[i][k]+=d[i][j]*c[j][k];                           // final result E= D*C
                }
            }
        }
    }
    t2_par=omp_get_wtime();                                             // stop recording the time taken for core multiplication
    Ttotal_par=(double) t2_par-t1_par;
    faster= time_taken/(Ttotal_par);
    
    /**
     for(int i=0;i<n;i++){
     for(int j=0;j<n;j++){
     //cout<< e[i][j]<<" ";
     }
     cout<<endl;
     }
     **/
    cout<<"\e[1m CPU time (Parallel code with "<<nthreads<<" threads) \e[0m"<<Ttotal_par<<endl;
    
    cout<<endl;
    cout<<"\e[1m The parallel code is "<<faster<<" times faster than sequential code \e[0m"<<endl;
    
    
    return(0);
}





