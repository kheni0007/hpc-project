/**
 Memebers of this group:
 Jonathan E. Dawson - 217203768
 Dileep Kumar Tentu - 217205583
 Vishal Kheni - 217205316
 Moulali Penchikalapalli  - 217205137
 Sweetyben Vekariya- 216206022
 
 This C++ code creates a matrix A filled with random numbers uniformly distributed in the interval [0,1].
 The size of the matrix is 1000 by 5.
 The matrix is created using both sequential or parallel code.
 
 **/

#include<iostream>
#include <stdlib.h> /* srand, rand */
#include <fstream>

using namespace std;

#define n 1000              // matrix with 1000 rows
#define m 5                // matrix with 5 columns


int main(int argc, char* argv[])
{
    
    int s;
    cout<<"\e[1m Random matrix generation: Enter 1 for using Sequential code or 2 Parallel code  : \e[0m";   //request for Sequential or Parallel computing
    cin>> s;
    
    
    switch(s)                   // switch statement for evaulate the two cases (sequential or parallel) based on user input
    {
        case 1:                                     // case Sequential
        {
            
            float a[n][m];                  // floating type for matrix element to allow for decimal numbers between 0 and 1
            float k;
            srand(321);                 // initialize the seed for the random number generator
            
            for(int i=0;i<n;i++){
                
                for(int j=0;j<m;j++){
                    
                    k = rand() % 100;               // generate random number in the range 0 to 99
                    a[i][j] = k/100;                // generate random number in the range 0 to 1
                }
            }
            cout<<"\e[1m Matrix created using sequential code (first and last five rows only printed):\e[0m";
            cout<<endl;
            cout<<"A=";
            cout<<endl;
            for(int i=0;i<5;i++){                       // print the first five rows of the matrix
                for(int j=0;j<m;j++){
                    cout<< a[i][j]<<"        ";
                }
                cout<<endl;
            }
            cout<<endl<<endl;
            for(int i=n-5;i<n;i++){                         // print the last five rows of the matrix
                for(int j=0;j<m;j++){
                    cout<< a[i][j]<<"        ";
                }
                cout<<endl;
            }
            
            return(0);
            break;
        }
        case 2:                                     // case Parallel
        {
            float a[n][m];
            float k;
            srand(321);
#pragma omp parallel                               // parallelization starts
            {
#pragma omp for                                    // parallelizing the 'for' loops
                for(int i=0;i<n;i++){
                    
                    for(int j=0;j<m;j++){
                        
                        k = rand() % 100;
                        a[i][j] = k/100;
                    }
                }
                
            }
            cout<<"\e[1m Matrix created using parallel code (first and last five rows only printed): \e[0m";
            cout<<endl;
            cout<<"A=";
            cout<<endl;

            
            for(int i=0;i<5;i++){
                for(int j=0;j<m;j++){
                    cout<< a[i][j]<<"        ";
                }
                cout<<endl;
            }
            cout<<endl<<endl;
            for(int i=n-5;i<n;i++){
                for(int j=0;j<m;j++){
                    cout<< a[i][j]<<"        ";
                }
                cout<<endl;
            }
            
            return(0);
            break;
        }
            
    }
    
    
}

