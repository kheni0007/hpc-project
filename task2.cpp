/**
Memebers of the group
Jonathan E. Dawson - 217203768
Dileep Kumar Tentu - 217205583
Vishal Kheni - 217205316
Moulali Penchikalapalli  - 217205137
Sweetyben Vekariya- 216206022
 
This code computes double matrix multiplication of three matrices (A(4by3),B(3by5) and C(5by4)).
Compting done using both sequential and parallel code.
User input is asked for at the beginnning for which of the two to be used to compute matrix multiplication.
The final result is stored in matrix E which is a 4 by 4 matrix.
**/

#include<iostream>
#include<stdlib.h>
#include<omp.h>

using namespace std;

#define n 4              // matrix with 1000 rows
#define m 3                // matrix with 5 columns
#define p 5              // matrix with 1000 rows

int main( )
{
    
    int s;
    int a[n][m],b[m][p],c[p][n],d[n][p],e[n][n];
    
    cout<<"\e[1m Matrix multiplication. Enter 1 for Sequential computing or 2 Parallel computing  : \e[0m";     //request for Sequential or Parallel computing
    cin>> s;
    
    switch(s)
    {
        case 1:
        {
            int k=1;
            for(int i=0;i<n;i++){
                for(int j=0;j<m;j++){
                    a[i][j]=k;
                    k++;
                }
            }
            k=1;
            for(int i=0;i<m;i++){
                for(int j=0;j<p;j++){
                    
                    b[i][j]=k;
                    k++;
                }
            }
            k=1;
            for(int i=0;i<p;i++){
                for(int j=0;j<n;j++){
                    c[i][j]=k;
                    k++;
                }
            }
            for(int i=0;i<n;i++){
                for(int j=0;j<p;j++){
                    d[i][j]=0;
                }
            }
            for(int i=0;i<n;i++){
                for(int j=0;j<n;j++){
                    e[i][j]=0;
                }
            }
            
            //serial multiplication
            for(int i=0;i<n;i++){
                for(int k=0;k<p;k++){
                    for(int j=0;j<m;j++){
                        d[i][k]+=a[i][j]*b[j][k];
                    }
                }
            }
            for(int i=0;i<n;i++){
                for(int k=0;k<n;k++){
                    for(int j=0;j<p;j++){
                        e[i][k]+=d[i][j]*c[j][k];
                    }
                }
            }
            
            cout<<"\e[1m This is a matrix multiplication (sequential code) : \e[0m";
            cout<<endl;
            cout<<"\e[1m A= \e[0m";
            cout<<endl;
            for(int i=0;i<n;i++){
                for(int j=0;j<m;j++){
                    cout<< a[i][j]<<" ";                               // print matrix A on output
                }
                cout<<endl;
            }
            cout<<endl;
            
            cout<<"\e[1m B= \e[0m";
            cout<<endl;
            for(int i=0;i<m;i++){
                for(int j=0;j<p;j++){
                    cout<< b[i][j]<<" ";                             // print matrix B on output
                }
                cout<<endl;
            }
            cout<<endl;
            
            cout<<"\e[1m C= \e[0m";
            cout<<endl;
            for(int i=0;i<p;i++){
                for(int j=0;j<n;j++){
                    cout<< b[i][j]<<" ";                             // print matrix B on output
                }
                cout<<endl;
            }
            cout<<endl;
            
            cout<<"\e[1m (AB)C= \e[0m";
            cout<<endl;
            for(int i=0;i<n;i++){
                for(int j=0;j<n;j++){
                    cout<< e[i][j]<<" ";                            // print matrix C on output
                }
                cout<<endl;
            }
            cout<<endl;
            
            
            return(0);
        }
            
        case 2:
        {
            int k=1;
            int nthreads;
            for(int i=0;i<n;i++){
                for(int j=0;j<m;j++){
                    a[i][j]=k;
                    k++;
                }
            }
            k=1;
            for(int i=0;i<m;i++){
                for(int j=0;j<p;j++){
                    b[i][j]=k;
                    k++;
                }
            }
            k=1;
            for(int i=0;i<p;i++){
                for(int j=0;j<n;j++){
                    c[i][j]=k;
                    k++;
                }
            }
            for(int i=0;i<n;i++){
                for(int j=0;j<p;j++){
                    d[i][j]=0;
                }
            }
            for(int i=0;i<n;i++){
                for(int j=0;j<n;j++){
                    e[i][j]=0;
                }
            }
            //parallel multiplication
#pragma omp parallel for
            for(int i=0;i<n;i++){
                for(int k=0;k<p;k++){
                    for(int j=0;j<m;j++){
                        d[i][k]+=a[i][j]*b[j][k];
                    }
                }
            }
#pragma omp parallel for
            for(int i=0;i<n;i++){
                for(int k=0;k<n;k++){
                    nthreads = omp_get_num_threads();
                    for(int j=0;j<p;j++){
                        e[i][k]+=d[i][j]*c[j][k];
                    }
                }
            }
            cout<<"\e[1m This is a matrix multiplication (parallel code) : \e[0m";
            cout<<endl;
            
            cout<<"\e[1m NUMBER of threads \e[0m" <<nthreads<<endl;
            cout<<endl;
            
            cout<<"\e[1m A=\e[0m";
            cout<<endl;
            for(int i=0;i<n;i++){
                for(int j=0;j<m;j++){
                    cout<< a[i][j]<<" ";                               // print matrix A on output
                }
                cout<<endl;
            }
            cout<<endl;
            
            cout<<"\e[1m B=\e[0m";
            cout<<endl;
            for(int i=0;i<m;i++){
                for(int j=0;j<p;j++){
                    cout<< b[i][j]<<" ";                             // print matrix B on output
                }
                cout<<endl;
            }
            cout<<endl;
            
            cout<<"\e[1m C=\e[0m";
            cout<<endl;
            for(int i=0;i<p;i++){
                for(int j=0;j<n;j++){
                    cout<< b[i][j]<<" ";                             // print matrix B on output
                }
                cout<<endl;
            }
            cout<<endl;
            
            cout<<"\e[1m(AB)C=\e[0m";
            cout<<endl;
            for(int i=0;i<n;i++){
                for(int j=0;j<n;j++){
                    cout<< e[i][j]<<" ";                            // print matrix C on output
                }
                cout<<endl;
            }
            cout<<endl;
            
            return(0);
        }
            
    }
    
}



