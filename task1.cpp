/**
 Memebers of the group
 Jonathan E. Dawson - 217203768
 Dileep Kumar Tentu - 217205583
 Vishal Kheni - 217205316
 Moulali Penchikalapalli  - 217205137
 Sweetyben Vekariya- 216206022
 
 This C++ code implments the sequential and parallel computing algorithm for multiplication of two matrices (A and B).
 The code on execution requests for users choice for sequential or parallel computing.
 The size of each matrix (A and B) is determined by the user input of rows and columns when prompted on code execution.
 The column of matrix A and row of matrix B is fixed and set to be equal to each other (matrix product rule).
 Each matrix (A and B) consists of entries that increasing whole numbers.
 The result of multiplication of the two matrices A and B is stored in C.
 
 **/


#include<iostream>
#include<stdlib.h>
#include<omp.h>

using namespace std;

#define n 6             // row for matrix A
#define m 6                // column of matrix A
#define p 6                // column of matrix B

int main( )
{
    
    int s;

    int a[n][m],b[m][p],c[n][p];                            // matrix as 2by2 array declared
    int k=1;                                                // counter initialized for matrix entries
    
    cout<<"\e[1mMatrix multiplication. Enter 1 for Sequential computing or 2 Parallel computing  : \e[0m";     //request for Sequential or Parallel computing
    cin>> s;
    
    switch(s)
    {
            
        case 1:
        {
            
            for(int i=0;i<n;i++){
                for(int j=0;j<m;j++){
                    a[i][j]=k;                                     // matrix A and B initialized with increasing whole numbers
                    b[i][j]=k;
                    c[i][j]= 0;                                    // matrix C=AB initialized to 0
                    k++;
                }
            }
            
            for(int i=0;i<n;i++){                               // FOR loop section where matrix multiplication A*B is performed and stored into C
                for(int k=0;k<p;k++){
                    for(int j=0;j<m;j++){
                        c[i][k]+=a[i][j]*b[j][k];
                    }
                }
            }
            
            cout<<"\e[1mThis is a matrix multiplication (sequential code) :\e[0m ";
            cout<<endl;
            cout<<"\e[1mA=\e[0m";
            cout<<endl;
            for(int i=0;i<n;i++){
                for(int j=0;j<m;j++){
                    cout<< a[i][j]<<" ";                               // print matrix A on output
                }
                cout<<endl;
            }
            cout<<endl;
            
            cout<<"\e[1mB=\e[0m";
            cout<<endl;
            for(int i=0;i<m;i++){
                for(int j=0;j<p;j++){
                    cout<< b[i][j]<<" ";                             // print matrix B on output
                }
                cout<<endl;
            }
            cout<<endl;
            
            cout<<"\e[1mAB=\e[0m";
            cout<<endl;
            for(int i=0;i<n;i++){
                for(int j=0;j<p;j++){
                    cout<< c[i][j]<<" ";                            // print matrix C on output
                }
                cout<<endl;
            }
            cout<<endl;
            
            return(0);
        }
            
        case 2:
        {
            int nthreads;                                          // nthreads will store the value for total number of threads
            
            for(int i=0;i<n;i++){
                for(int j=0;j<m;j++){
                    a[i][j]=k;                                     // matrix A and B initialized with increasing whole numbers
                    b[i][j]=k;
                    c[i][j]= 0;                                    // matrix C=AB initialized to 0
                    k++;
                }
            }
            
#pragma omp parallel for                             // declaration for parallel distribution and evaluation of the FOR loop
            
            for(int i=0;i<n;i++){                               // FOR loop section where matrix multiplication A*B is performed and stored into C
                nthreads = omp_get_num_threads();               // get the value for number of threads in the operating system
                for(int k=0;k<p;k++){
                    for(int j=0;j<m;j++){
                        c[i][k]+=a[i][j]*b[j][k];
                    }
                }
            }
            cout<<"\e[1mNUMBER of threads \e[0m" <<nthreads<<endl;            // prints total number of threads
            cout<<endl;
            cout<<"\e[1mThis is a matrix multiplication (parallel code) : \e[0m";
            cout<<endl;
            cout<<"\e[1mA=\e[0m";
            cout<<endl;
            for(int i=0;i<n;i++){
                for(int j=0;j<m;j++){
                    cout<< a[i][j]<<" ";                               // print matrix A on output
                }
                cout<<endl;
            }
            cout<<endl;
            
            cout<<"\e[1mB=\e[0m";
            cout<<endl;
            for(int i=0;i<m;i++){
                for(int j=0;j<p;j++){
                    cout<< b[i][j]<<" ";                             // print matrix B on output
                }
                cout<<endl;
            }
            cout<<endl;
            
            cout<<"\e[1mAB=\e[0m";
            cout<<endl;
            for(int i=0;i<n;i++){
                for(int j=0;j<p;j++){
                    cout<< c[i][j]<<" ";                            // print matrix C on output
                }
                cout<<endl;
            }
            cout<<endl;
            
            return(0);
            
            
        }
            
            
            
    }
    
    
    
}





